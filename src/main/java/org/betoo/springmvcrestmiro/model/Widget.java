package org.betoo.springmvcrestmiro.model;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.text.SimpleDateFormat;

public class Widget {
	 
	private UUID id;
	private Date modificationDate;
	private int positionX;
	private int positionY;
	private int width;
	private int height;
	private int zIndex;
	
    // This default constructor is required if there are other constructors.
    public Widget() {
 
    }
 
    public Widget(int positionX, int positionY, int width, int height,
    		int zIndex) {
    	this.id = UUID.randomUUID();
        this.modificationDate = new Date();
        this.positionX = positionX;
        this.positionY = positionY;
        this.width = width;
        this.height = height;
        this.zIndex = zIndex;
    }
 
    public UUID getId() {
        return this.id;
    }
 
   public void setId(UUID id) {
        this.id = id;
    }

    public Date getModificationDate() {
    	return this.modificationDate;
    }

    @JsonProperty("modificationDate")    
    public String getModificationDateString() {
    	SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss z");
        return dateFormat.format(this.modificationDate);
    }
    
    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }
    
    public int getPositionX() {
        return this.positionX;
    }
 
    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }
    
    public int getPositionY() {
        return this.positionY;
    }
 
    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }
    
    public int getWidth() {
        return this.width;
    }
 
    public void setWidth(int width) {
        this.width = width;
    }
    
    public int getHeight() {
        return this.height;
    }
 
    public void setHeight(int height) {
        this.height = height;
    }    

    public int getZIndex() {
        return this.zIndex;
    }
 
    public void setZIndex(int zIndex) {
        this.zIndex = zIndex;
    }
}
