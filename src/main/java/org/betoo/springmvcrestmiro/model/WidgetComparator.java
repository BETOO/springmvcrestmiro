package org.betoo.springmvcrestmiro.model;

import java.util.Comparator;

import org.betoo.springmvcrestmiro.model.Widget;;

public class WidgetComparator implements Comparator<Widget>{

	@Override
	public int compare(Widget widget0, Widget widget1) {
		return widget0.getZIndex() - widget1.getZIndex();
	}

}
