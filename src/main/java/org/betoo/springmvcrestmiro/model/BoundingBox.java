package org.betoo.springmvcrestmiro.model;

public class BoundingBox {
	 
	private int leftUpPointX;
	private int leftUpPointY;
	private int rightDownPointX;
	private int rightDownPointY;
	
    // This default constructor is required if there are other constructors.
    public BoundingBox() {
 
    }
 
    public BoundingBox(int leftUpPointX, int leftUpPointY, int rightDownPointX,
    		int rightDownPointY) {
        this.leftUpPointX = leftUpPointX;
        this.leftUpPointY = leftUpPointY;
        this.rightDownPointX = rightDownPointX;
        this.rightDownPointY = rightDownPointY;
    }
    
    public int getLeftUpPointX() {
        return this.leftUpPointX;
    }
 
    public void setLeftUpPointX(int leftUpPointX) {
        this.leftUpPointX = leftUpPointX;
    }
    
    public int getLeftUpPointY() {
        return this.leftUpPointY;
    }
 
    public void setLeftUpPointY(int leftUpPointY) {
        this.leftUpPointY = leftUpPointY;
    }
    
    public int getRightDownPointX() {
        return this.rightDownPointX;
    }
 
    public void setRightDownPointX(int rightDownPointX) {
        this.rightDownPointX = rightDownPointX;
    }
    
    public int getRightDownPointY() {
        return this.rightDownPointY;
    }
 
    public void setRightDownPointY(int rightDownPointY) {
        this.rightDownPointY = rightDownPointY;
    }    
}
