package org.betoo.springmvcrestmiro.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
 
@Configuration
 
@ComponentScan("org.betoo.springmvcrestmiro.*")
 
public class ApplicationContextConfig {
 
 
   // No need ViewSolver
  
  
 
   // Other declarations if needed ...
  
}