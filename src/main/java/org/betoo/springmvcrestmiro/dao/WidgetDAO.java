package org.betoo.springmvcrestmiro.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.betoo.springmvcrestmiro.model.Widget;
import org.springframework.stereotype.Repository;
 
@Repository
public class WidgetDAO {
 
	private static final ReentrantReadWriteLock WidgetMapLock = 
			new ReentrantReadWriteLock(true);
    private static final Map<UUID, Widget> WidgetMap = new HashMap<UUID, Widget>();
    private static int maxZIndex = 0;
    static {
        initEmps();
    }
 
    private static void initEmps() {
        /*Widget widget1 = new Widget(0, 0, 50, 50, 1);
        Widget widget2 = new Widget(44, 33, 10, 40, 2);
        Widget widget3 = new Widget(33, 44, 40, 90, 3);
 
        if (widget1.getZIndex() > maxZIndex)
        	maxZIndex = widget1.getZIndex();
        WidgetMap.put(widget1.getId(), widget1);
        if (widget2.getZIndex() > maxZIndex)
        	maxZIndex = widget2.getZIndex();
        WidgetMap.put(widget2.getId(), widget2);
        if (widget3.getZIndex() > maxZIndex)
        	maxZIndex = widget3.getZIndex();
        WidgetMap.put(widget3.getId(), widget3);*/
    }
 
    public Widget getWidget(UUID id) {
    	Widget gettedWidget = null;
    	WidgetMapLock.readLock().lock();
    	try {
    		gettedWidget = WidgetMap.get(id);
    	}
    	finally {
    		WidgetMapLock.readLock().unlock();
    	}
    	return gettedWidget;
    }
 
    public Widget addWidget(Widget addedWidget) {
    	int inputZIndex = addedWidget.getZIndex();
    	WidgetMapLock.writeLock().lock();
    	try {
		if ((inputZIndex <= 0) || (inputZIndex > maxZIndex))
    	{
    		maxZIndex++;
    		addedWidget.setZIndex(maxZIndex);
    	}
    	else
    	{
            Collection<Widget> widgetsCollection = WidgetMap.values();
            for (Widget selectedWidget : widgetsCollection)
            {
            	int selectedZIndex = selectedWidget.getZIndex();
				if (selectedZIndex >= inputZIndex)
            	{
					selectedWidget.setZIndex(selectedZIndex+1);
            	}
            }
            maxZIndex++;
    	}
    	WidgetMap.put(addedWidget.getId(), addedWidget);
    	}
    	finally {
    		WidgetMapLock.writeLock().unlock();
    	}
        return addedWidget;
    }
 
    public Widget updateWidget(Widget oldWidget, Widget widget) {
    	int oldZIndex = oldWidget.getZIndex();
    	int zIndex = widget.getZIndex();
    	WidgetMapLock.writeLock().lock();
    	try {
    	if ((zIndex <= 0) || (zIndex > maxZIndex))
		{
    		widget.setZIndex(maxZIndex);
    		zIndex = maxZIndex;
		}
		
    	int difference = zIndex - oldZIndex;
    	if (difference > 0)
    	{
    		Collection<Widget> widgetsCollection = WidgetMap.values();
            for (Widget selectedWidget : widgetsCollection)
            {
            	int selectedZIndex = selectedWidget.getZIndex();
    			if ((selectedZIndex > oldZIndex) && (selectedZIndex <= zIndex))
            	{
    				selectedWidget.setZIndex(selectedZIndex-1);
            	}
            }
    	}
    	if (difference < 0)
    	{
    		Collection<Widget> widgetsCollection = WidgetMap.values();
            for (Widget selectedWidget : widgetsCollection)
            {
            	int selectedZIndex = selectedWidget.getZIndex();
    			if ((selectedZIndex >= zIndex) && (selectedZIndex < oldZIndex))
            	{
    				selectedWidget.setZIndex(selectedZIndex+1);
            	}
            }
    	}
    	WidgetMap.put(widget.getId(), widget);
    	}
    	finally {
    		WidgetMapLock.writeLock().unlock();
    	}
    	return widget;
    }
 
    public void deleteWidget(UUID id, Widget 
    		removableWidget) {
    	int removableZIndex = removableWidget.getZIndex();
    	WidgetMapLock.writeLock().lock();
    	try {
    	Collection<Widget> widgetsCollection = WidgetMap.values();
        for (Widget selectedWidget : widgetsCollection)
        {
        	int selectedZIndex = selectedWidget.getZIndex();
			if (selectedZIndex > removableZIndex)
        	{
				selectedWidget.setZIndex(selectedZIndex-1);
        	}
        }
        maxZIndex--;
    	WidgetMap.remove(id);
    }
	finally {
		WidgetMapLock.writeLock().unlock();
	}
    }
 
    public List<Widget> getAllWidgets() {
    	Collection<Widget> widgetsCollection = null;
    	WidgetMapLock.readLock().lock();
    	try {
    		widgetsCollection = WidgetMap.values();
    	}
    	finally {
    		WidgetMapLock.readLock().unlock();
    	}
    	List<Widget> widgetsList = new ArrayList<Widget>();
        widgetsList.addAll(widgetsCollection);
        widgetsList.sort(Comparator.comparing(Widget::getZIndex));
        return widgetsList;
    }

	public void Clear() {
		WidgetMapLock.writeLock().lock();
    	try {
    		WidgetMap.clear();
    		maxZIndex = 0;
    	}
    	finally {
    		WidgetMapLock.writeLock().unlock();
    	}
	}
}
