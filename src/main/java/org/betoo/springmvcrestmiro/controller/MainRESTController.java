package org.betoo.springmvcrestmiro.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.betoo.springmvcrestmiro.dao.WidgetDAO;
import org.betoo.springmvcrestmiro.model.BoundingBox;
import org.betoo.springmvcrestmiro.model.Widget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
 
@RestController
public class MainRESTController {
 
    @Autowired
    private WidgetDAO WidgetDAO;
 
    @RequestMapping("/")
    @ResponseBody
    public String welcome() {
        return "Welcome to RestTemplate Example.";
    }
 
    // URL:
    // http://localhost:8080/SpringMvcRestMiro/widgets
    // http://localhost:8080/SpringMvcRestMiro/widgets.xml
    // http://localhost:8080/SpringMvcRestMiro/widgets.json
    @RequestMapping(value = "/widgets", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public ResponseEntity<List<Widget>> getWidgetsFromBox(@RequestBody(required=false) BoundingBox boundingBox) {
    	if (boundingBox == null)
    	{
    		List<Widget> widgetsList = WidgetDAO.getAllWidgets();
            return new ResponseEntity<List<Widget>>(widgetsList, HttpStatus.OK);
    	}
    	
    	boolean isGoodRequest = isGoodBoundingBoxRequest(boundingBox);
    	if (!isGoodRequest)
    		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    	
		int leftUpPointX = boundingBox.getLeftUpPointX();
    	int leftUpPointY = boundingBox.getLeftUpPointY();
    	int rightDownPointX = boundingBox.getRightDownPointX();
    	int rightDownPointY = boundingBox.getRightDownPointY();
    	
        List<Widget> widgetsList = WidgetDAO.getAllWidgets();
        List<Widget> selectedWidgetsList = new ArrayList<Widget>();
        for (Widget selectedWidget : widgetsList)
        {
    		int positionX = selectedWidget.getPositionX();
        	int positionY = selectedWidget.getPositionY();
        	int width = selectedWidget.getWidth();
        	int height = selectedWidget.getHeight();
        	
        	if ((positionX < leftUpPointX) || (positionY < leftUpPointY))
        		continue;
        	
        	if ((positionX + width > rightDownPointX) || (positionY + height > rightDownPointY))
        		continue;
        	
        	selectedWidgetsList.add(selectedWidget);
        }
        
        return new ResponseEntity<List<Widget>>(selectedWidgetsList, HttpStatus.OK);
    }
    
    // URL:
    // http://localhost:8080/SpringMvcRestMiro/widget/{id}
    // http://localhost:8080/SpringMvcRestMiro/widget/{id}.xml
    // http://localhost:8080/SpringMvcRestMiro/widget/{id}.json
    @RequestMapping(value = "/widget/{id}", //
            method = RequestMethod.GET, //
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public ResponseEntity<Widget> getWidget(@PathVariable("id") UUID id) {
    	Widget widget = WidgetDAO.getWidget(id);
		if (widget == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		
		return new ResponseEntity<Widget>(widget, HttpStatus.OK);
    }
 
    // URL:
    // http://localhost:8080/SpringMvcRestMiro/widget
    // http://localhost:8080/SpringMvcRestMiro/widget.xml
    // http://localhost:8080/SpringMvcRestMiro/widget.json
    @RequestMapping(value = "/widget", //
            method = RequestMethod.POST, //
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public ResponseEntity<Widget> addWidget(@RequestBody Widget widget) {
    	boolean isGoodRequest = isGoodChangeWidgetRequest(widget);
    	if (!isGoodRequest)
    		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    	
    	widget.setModificationDate(new Date());
    	widget.setId(UUID.randomUUID());
        Widget addedWidget = WidgetDAO.addWidget(widget);
		return new ResponseEntity<Widget>(addedWidget, HttpStatus.OK);
    }
 
    // URL:
    // http://localhost:8080/SpringMvcRestMiro/widget/{id}
    // http://localhost:8080/SpringMvcRestMiro/widget/{id}.xml
    // http://localhost:8080/SpringMvcRestMiro/widget/{id}.json
    @RequestMapping(value = "/widget/{id}", //
            method = RequestMethod.PUT, //
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public ResponseEntity<Widget> updateWidget(@PathVariable("id") UUID id, @RequestBody Widget widget) {
    	boolean isGoodRequest = isGoodChangeWidgetRequest(widget);
    	if (!isGoodRequest)
    		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    	
    	Widget oldWidget = WidgetDAO.getWidget(id);
    	if (WidgetDAO.getWidget(id) == null)
    		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    	
    	widget.setModificationDate(new Date());
    	widget.setId(id);
        Widget updatedWidget = WidgetDAO.updateWidget(oldWidget, widget);
		return new ResponseEntity<Widget>(updatedWidget, HttpStatus.OK);
    }
 
    // URL:
    // http://localhost:8080/SpringMvcRestMiro/widget/{id}
    @RequestMapping(value = "/widget/{id}", //
            method = RequestMethod.DELETE, //
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public ResponseEntity<Widget> deleteWidget(@PathVariable("id") UUID id) {
    	Widget widget = WidgetDAO.getWidget(id);
		if (widget == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		
    	WidgetDAO.deleteWidget(id, widget);
		return new ResponseEntity<>(HttpStatus.OK);
    }

	private boolean isGoodChangeWidgetRequest(Widget widget) {
    	int width = widget.getWidth();
    	int height = widget.getHeight();
    	boolean isGoodRequest = (width > 0) && (height > 0);
		return isGoodRequest;
	}
	
	private boolean isGoodBoundingBoxRequest(BoundingBox boundingBox) {
		int leftUpPointX = boundingBox.getLeftUpPointX();
    	int leftUpPointY = boundingBox.getLeftUpPointY();
    	int rightDownPointX = boundingBox.getRightDownPointX();
    	int rightDownPointY = boundingBox.getRightDownPointY();
    	boolean isGoodRequest = (rightDownPointX > leftUpPointX) 
    			&& (rightDownPointY > leftUpPointY);
		return isGoodRequest;
	}
}
