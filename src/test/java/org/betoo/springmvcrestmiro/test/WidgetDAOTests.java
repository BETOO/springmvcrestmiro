package org.betoo.springmvcrestmiro.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.betoo.springmvcrestmiro.dao.WidgetDAO;
import org.betoo.springmvcrestmiro.model.Widget;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class WidgetDAOTests {
	private static WidgetDAO WidgetDAOTest  = new WidgetDAO();
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		WidgetDAOTest  = new WidgetDAO();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		WidgetDAOTest.Clear();
		WidgetDAOTest = null;
	}

	@Test
	void test_add() {
		Widget widget1 = new Widget(0, 0, 50, 50, 1);
        Widget widget2 = new Widget(44, 33, 10, 40, 2);
        Widget widget3 = new Widget(33, 44, 40, 90, 3);
		WidgetDAOTest.addWidget(widget1);
		WidgetDAOTest.addWidget(widget2);
		WidgetDAOTest.addWidget(widget3);
		Widget widget4 = new Widget(0, 0, 50, 50, 2);
        Widget widget5 = new Widget(44, 33, 10, 40, -1);
        Widget widget6 = new Widget(33, 44, 40, 90, 99);
		WidgetDAOTest.addWidget(widget4);
		WidgetDAOTest.addWidget(widget5);
		WidgetDAOTest.addWidget(widget6);
		
		List<Widget> widgetsList = WidgetDAOTest.getAllWidgets();
		assertEquals(6, widgetsList.size());
	}
	
	@Test
	void test_getAll() {
		List<Widget> widgetsList = WidgetDAOTest.getAllWidgets();
		String zIndexSequence = "";
		for (Widget selectedWidget : widgetsList)
		{
			int zIndex = selectedWidget.getZIndex();
			zIndexSequence += zIndex;
		}
		assertEquals("123456", zIndexSequence);
	}
	
	@Test
	void test_get() {
		List<Widget> widgetsList = WidgetDAOTest.getAllWidgets();
		int zIndex=0;
		for (Widget selectedWidget : widgetsList)
		{
			zIndex++;
			UUID id = selectedWidget.getId();
			Widget gotWidget = WidgetDAOTest.getWidget(id);
			assertEquals(zIndex, gotWidget.getZIndex());
		}
		Widget badWidget = WidgetDAOTest.getWidget(UUID.randomUUID());
		assertEquals(null, badWidget);
	}
	
	@Test
	void test_update() {
		ArrayList<UUID> widgetsUUIDList = getUUIDList();
		
		Widget gotWidget = WidgetDAOTest.getWidget(widgetsUUIDList.get(0));
		Widget widget1 = new Widget(11, 22, 111, 112, 6);
		widget1.setModificationDate(new Date());
    	widget1.setId(widgetsUUIDList.get(0));
		WidgetDAOTest.updateWidget(gotWidget, widget1);
		gotWidget = WidgetDAOTest.getWidget(widgetsUUIDList.get(0));
		assertEquals(11, gotWidget.getPositionX());
		assertEquals(112, gotWidget.getHeight());
		assertEquals(6, gotWidget.getZIndex());
		
		gotWidget = WidgetDAOTest.getWidget(widgetsUUIDList.get(5));
		Widget widget2 = new Widget(11, 22, 111, 112, 1);
		widget2.setModificationDate(new Date());
    	widget2.setId(widgetsUUIDList.get(5));
    	WidgetDAOTest.updateWidget(gotWidget, widget2);
		gotWidget = WidgetDAOTest.getWidget(widgetsUUIDList.get(5));
		assertEquals(22, gotWidget.getPositionY());
		assertEquals(111, gotWidget.getWidth());
		assertEquals(1, gotWidget.getZIndex());
		
		test_getAll();
	}

	private ArrayList<UUID> getUUIDList() {
		List<Widget> widgetsList = WidgetDAOTest.getAllWidgets();
		ArrayList<UUID> widgetsUUIDList = new ArrayList<UUID>();
		for (Widget selectedWidget : widgetsList)
		{
			UUID id = selectedWidget.getId();
			widgetsUUIDList.add(id);
		}
		return widgetsUUIDList;
	}
}
