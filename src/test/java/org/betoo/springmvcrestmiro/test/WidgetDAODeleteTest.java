package org.betoo.springmvcrestmiro.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.betoo.springmvcrestmiro.dao.WidgetDAO;
import org.betoo.springmvcrestmiro.model.Widget;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class WidgetDAODeleteTest {
	private static WidgetDAO WidgetDAOTest  = new WidgetDAO();
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		WidgetDAOTest  = new WidgetDAO();
		
		Widget widget1 = new Widget(0, 0, 50, 50, 1);
        Widget widget2 = new Widget(44, 33, 10, 40, 2);
        Widget widget3 = new Widget(33, 44, 40, 90, 3);
		WidgetDAOTest.addWidget(widget1);
		WidgetDAOTest.addWidget(widget2);
		WidgetDAOTest.addWidget(widget3);
		Widget widget4 = new Widget(0, 0, 50, 50, 2);
        Widget widget5 = new Widget(44, 33, 10, 40, -1);
        Widget widget6 = new Widget(33, 44, 40, 90, 99);
		WidgetDAOTest.addWidget(widget4);
		WidgetDAOTest.addWidget(widget5);
		WidgetDAOTest.addWidget(widget6);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		WidgetDAOTest.Clear();
		WidgetDAOTest = null;
	}

	@Test
	void test_delete() {
		ArrayList<UUID> widgetsUUIDList = getUUIDList();
		
		Widget removableWidget = WidgetDAOTest.getWidget(widgetsUUIDList.get(0));
		WidgetDAOTest.deleteWidget(widgetsUUIDList.get(0), removableWidget);
		removableWidget = WidgetDAOTest.getWidget(widgetsUUIDList.get(3));
		WidgetDAOTest.deleteWidget(widgetsUUIDList.get(3), removableWidget);
		removableWidget = WidgetDAOTest.getWidget(widgetsUUIDList.get(5));
		WidgetDAOTest.deleteWidget(widgetsUUIDList.get(5), removableWidget);
		
		List<Widget> widgetsList = WidgetDAOTest.getAllWidgets();
		assertEquals(3, widgetsList.size());
		
		String zIndexSequence = "";
		for (Widget selectedWidget : widgetsList)
		{
			int zIndex = selectedWidget.getZIndex();
			zIndexSequence += zIndex;
		}
		assertEquals("123", zIndexSequence);
	}
	
	private ArrayList<UUID> getUUIDList() {
		List<Widget> widgetsList = WidgetDAOTest.getAllWidgets();
		ArrayList<UUID> widgetsUUIDList = new ArrayList<UUID>();
		for (Widget selectedWidget : widgetsList)
		{
			UUID id = selectedWidget.getId();
			widgetsUUIDList.add(id);
		}
		return widgetsUUIDList;
	}
}
